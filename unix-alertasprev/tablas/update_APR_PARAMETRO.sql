use alertasprev
go

UPDATE APR_PARAMETRO
set PAR_DESCRIPCION = 'Infracciones > 2 por no pago previsional, sin incl'
	,PAR_ADICIONAL = '2'
WHERE 
PAR_CODIGO = '60'
go

UPDATE APR_PARAMETRO
SET PAR_ADICIONAL= 'R'
WHERE 
PAR_CODIGO IN ('EE60                ' , 'GGEE60              ')
go