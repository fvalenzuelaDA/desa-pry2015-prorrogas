IF OBJECT_ID('dbo.vw_APR_Datos_Cliente') IS NOT NULL
BEGIN
    DROP VIEW dbo.vw_APR_Datos_Cliente
    IF OBJECT_ID('dbo.vw_APR_Datos_Cliente') IS NOT NULL
        PRINT '<<< FAILED DROPPING VIEW dbo.vw_APR_Datos_Cliente >>>'
    ELSE
        PRINT '<<< DROPPED VIEW dbo.vw_APR_Datos_Cliente >>>'
END
go
CREATE VIEW vw_APR_Datos_Cliente AS
SELECT  
     UU.PERIODO_PROC     
    ,UU.RUT          
    ,UU.CLI_DV       
    ,UU.CLI_NOMBRE  
	,CASE 
		WHEN RA.RES_COLOR='A' THEN 'Amarillo' 
		WHEN RA.RES_COLOR='R' THEN 'Rojo' 
		ELSE ' '
	END AS COLOR
	,CASE 
		WHEN RA.RES_IMPACTO='A' THEN 'Alto Impacto' 
		WHEN RA.RES_IMPACTO='M' THEN 'Impacto Moderado' 
		ELSE ' '
	END AS IMPACTO
    ,UU.CLI_COD_EJEC 
    ,UU.CLI_COD_BANCO
    ,UU.CLI_COD_BANCA
    ,UU.CLI_DESC_REG 
    ,UU.CLI_DESC_PLAT
    ,CX.CPL_COD_CLA_BCI
    ,CX.CPL_FEC_CAL
	,DB.BCI_MTO_DEU_DIR_BCI
	,BL.FECHA_BAL
	,BL.BIF_TIP_BAL
    ,CX.CPL_FEC_ULT_APC      
	,CX.CPL_IND_CLI_COS_EPY
    ,UU.CLI_IND_CCEE 
	,HR.RES_TOT_ALERTAS as RES_TOT_ALERTAS_ANT
	,RA.RES_TOT_ALT_ROJ
	,RA.RES_TOT_ALT_AMA
FROM  
    APR_HIST_CLIENTE UU
	LEFT JOIN (SELECT DISTINCT RUT,PERIODO_PROC,CPL_COD_CLA_BCI,CPL_FEC_CAL,CPL_FEC_ULT_APC,CPL_IND_CLI_COS_EPY FROM APR_HIST_COMPLEMENTO)  CX ON UU.RUT = CX.RUT AND UU.PERIODO_PROC=CX.PERIODO_PROC
	LEFT JOIN APR_HIST_DEUDABCI	DB ON UU.RUT = DB.RUT AND UU.PERIODO_PROC=DB.PERIODO_PROC
	LEFT JOIN APR_HIST_RESUMEN_ALERTA RA ON UU.RUT = RA.RUT AND UU.PERIODO_PROC=RA.PERIODO_PROC
    LEFT JOIN vw_APR_Hist_Riesgo HR ON UU.RUT = HR.RUT 
	LEFT JOIN (SELECT PERIODO_PROC, RUT, MAX(BIF_FEC_BAL) FECHA_BAL, max(BIF_TIP_BAL) AS BIF_TIP_BAL FROM APR_BALANCE group by PERIODO_PROC, RUT) BL ON  UU.RUT = BL.RUT AND UU.PERIODO_PROC=BL.PERIODO_PROC
WHERE
    UU.PERIODO_PROC=(select CONVERT(INT,PAR_ADICIONAL) from APR_PARAMETRO WHERE PAR_CONCEPTO = 'PERIODOS' AND PAR_CODIGO ='PERIODOACT')
go
GRANT SELECT ON dbo.vw_APR_Datos_Cliente TO consultas
go
GRANT DELETE ON dbo.vw_APR_Datos_Cliente TO mantencion
go
GRANT INSERT ON dbo.vw_APR_Datos_Cliente TO mantencion
go
GRANT SELECT ON dbo.vw_APR_Datos_Cliente TO mantencion
go
GRANT UPDATE ON dbo.vw_APR_Datos_Cliente TO mantencion
go
IF OBJECT_ID('dbo.vw_APR_Datos_Cliente') IS NOT NULL
    PRINT '<<< CREATED VIEW dbo.vw_APR_Datos_Cliente >>>'
ELSE
    PRINT '<<< FAILED CREATING VIEW dbo.vw_APR_Datos_Cliente >>>'
go	