use alertasprev
go

/* **********************************************************************************************************************/
/* Nombre SP                     : sp_alerta_calc_act                                                                   */
/* Nombre BD                     : alertasprev                                                                          */
/* Tipo de ejecucion             : Batch                                                                                */
/* Fecha creacion                : 14/10/2014                                                                           */
/* Fecha Modificado				 : 19/08/2016                                                                           */
/* Autor                         : Gabriel Martinez. (ADA LTDA)                                                         */
/* Modificado					 : Francisco Valenzuela                                                                 */
/* Objetivos                     : Proceso que realiza el calculo de las alertas de preventivas para el periodo actual  */
/* Canal de ejecucion            : Control-M                                                                            */
/* Psrsmetros entrada            : No aplica                                                                            */
/* Retorno                       : Datos en tablas fisicas                                                              */
/* Ejemplo de ejecucion          : exec sp_alerta_calc_act                                                              */
/* **********************************************************************************************************************/
/* Fecha Modificado				 : 19/08/2016  																			*/
/* Modificado					 : Francisco Valenzuela. (ADA LTDA)                                                     */
/* Objetivos                     : Se agregam y eliminan indicadores al proceso que realiza el calculo de las alertas   */
/*									de preventivas para el periodo actual  												*/
/* **********************************************************************************************************************/
/* Fecha Modificado				 : 06/09/2016  																			*/
/* Modificado					 : Francisco Valenzuela. (ADA LTDA)                                                     */
/* Objetivos                     : Se agregan valor deuda financiera, la cual antes se cargaba en cero					*/
/* **********************************************************************************************************************/
/* Fecha Modificado				 : 08/11/2016  																			*/
/* Modificado					 : Francisco Valenzuela. (ADA LTDA)                                                     */
/* Objetivos                     : Se modifica el tipo de dato a la columna BIF_IND_LQC 								*/
/* **********************************************************************************************************************/
/* Fecha Modificado				 : 27/12/2016  																			*/
/* Modificado					 : Francisco Valenzuela. (ADA LTDA)                                                     */
/* Objetivos                     : Se modifica los montos de balance, para que estos se encuentren proyectados			*/
/* **********************************************************************************************************************/
/* Fecha Modificado				 : 18/01/2017  																			*/
/* Modificado					 : Francisco Valenzuela. (ADA LTDA)                                                     */
/* Objetivos                     : Se modifica el indicador numero 28, ya que este en el calculo utilizaba 				*/
/* 								 : deuda financiera, y lo correcto es gastos financieros								*/
/* **********************************************************************************************************************/
/* Fecha Modificado				 : 05/08/2021 																			*/
/* Modificado					 : Francisco Valenzuela. (ADA LTDA)                                                     */
/* Objetivos                     : Se agrega alerta 120, indicador de marca prorroga, renegociado, renovacion, 			*/
/*									refinanciamiento																	*/
/* **********************************************************************************************************************/

IF OBJECT_ID('sp_alerta_calc_act') IS NOT NULL
BEGIN
    DROP PROCEDURE sp_alerta_calc_act
    IF OBJECT_ID('sp_alerta_calc_act') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE sp_alerta_calc_act >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE sp_alerta_calc_act >>>'
END
go
CREATE PROCEDURE sp_alerta_calc_act
AS
BEGIN
Print 'Inicio Procedimiento sp_alerta_calc_act'


Print 'Crea tabla temporal #banca_cliente'
CREATE TABLE #banca_cliente
(
	 RUT				int 	 		not null		
	,PERIODO_PROC		decimal(16,0)	not null
	,CLI_COD_BANCA		char(3)			null
	,PAR_CODIGO			char(20)		null
	,FEC_EJEC			datetime		null	
	,primary key (RUT,PERIODO_PROC)
)

	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #banca_cliente'
				RETURN -1
	END


Print 'Inserta Agrupacion de banca de clientes en la tabla #banca_cliente'
INSERT INTO #banca_cliente
SELECT 
	 a.RUT
	,a.PERIODO_PROC
	,a.CLI_COD_BANCA
	,b.PAR_CODIGO
	,a.FEC_EJEC
FROM 
        APR_HIST_CLIENTE a
LEFT JOIN 
		APR_PARAMETRO b
ON
    a.CLI_COD_BANCA = b.PAR_ADICIONAL
AND b.PAR_VIGENCIA = 'S' 
AND b.PAR_CONCEPTO = 'AGRBANCAS'
WHERE
	a.PERIODO_PROC = (SELECT convert(int,PAR_ADICIONAL) FROM APR_PARAMETRO where PAR_CODIGO = 'PERIODOACT' and PAR_VIGENCIA = 'S')


		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #banca_cliente'
			return -1
		END


/* **************************************** */
/*      		APR_HIST_COMPLEMENTO 				*/
/* **************************************** */

Print 'Crea tabla temporal #complemento_act'
CREATE TABLE #complemento_act
(
	 RUT					int 	 		not null		
	,PERIODO_PROC			decimal(16,0)	not null		
	,CPL_MTO_VTA_MES		decimal (18,4)	null
	,CPL_VAL_PI				decimal (18,6)	null
	,CPL_FEC_CAL			datetime		null
	,primary key (RUT,PERIODO_PROC)
)

	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #complemento_act'
				RETURN -1
	END


Print 'Insert a tabla #complemento_act datos de APR_HIST_COMPLEMENTO'
INSERT INTO #complemento_act
SELECT DISTINCT
     RUT
	,PERIODO_PROC
	,CPL_MTO_VTA_MES
	,CPL_VAL_PI		
	,CPL_FEC_CAL
FROM APR_HIST_COMPLEMENTO
WHERE 
	PERIODO_PROC = (SELECT convert(int,PAR_ADICIONAL) FROM APR_PARAMETRO where PAR_CODIGO = 'PERIODOACT' and PAR_VIGENCIA = 'S')

	
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #complemento_act'
			return -1
		END

/* ********************************************************** */
/*      		APR_HIST_COMPLEMENTO NIVEL OPERACION			*/
/* ********************************************************** */

Print 'Crea tabla temporal #complemento_act_ope'
CREATE TABLE #complemento_act_ope
(
	 RUT						int 	 		not null		
	,PERIODO_PROC				decimal(16,0)	not null		
	,CPL_MTO_VTA_MES			decimal (18,4)	null
	,CPL_VAL_PI					decimal (18,6)	null
	,CPL_FEC_CAL				datetime		null
	,CPL_ACCOUNT_NUM			char(18)	 null
	,CPL_ACCOUNT_MOD_NUM	varchar(18)		 null
	,CPL_MARCAR_ALR				int				null
	,CPL_CORR					int			not null
	,primary key (RUT,PERIODO_PROC,CPL_CORR)
)

	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #complemento_act_ope'
				RETURN -1
	END


Print 'Insert a tabla #complemento_act_ope datos de APR_HIST_COMPLEMENTO'
INSERT INTO #complemento_act_ope
SELECT
     RUT
	,PERIODO_PROC
	,CPL_MTO_VTA_MES
	,CPL_VAL_PI		
	,CPL_FEC_CAL
	,CPL_ACCOUNT_NUM
	,CPL_ACCOUNT_MOD_NUM
	,CPL_MARCAR_ALR
	,CPL_CORR
FROM APR_HIST_COMPLEMENTO
WHERE 
	PERIODO_PROC = (SELECT convert(int,PAR_ADICIONAL) FROM APR_PARAMETRO where PAR_CODIGO = 'PERIODOACT' and PAR_VIGENCIA = 'S')

	
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #complemento_act_ope'
			return -1
		END
		

/* *********************************************************************** */	
/* ************************** CALCULOS *********************************** */	
/* *********************************************************************** */


Print 'Crea tabla temporal #tabla_indicador'
CREATE TABLE #tabla_indicador
(
	 RUT				int 	 		not null		
	,COD_INDICADOR  	smallint    	not null
	,ESTADO				char (1)		null
	,primary key (RUT,COD_INDICADOR)
)

	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #tabla_indicador'
				RETURN -1
	END


Print 'Crea tabla temporal #tabla_indicador'
CREATE TABLE #tabla_indicador02
(
	 RUT				int 	 		not null		
	,COD_INDICADOR  	smallint    	not null
	,ESTADO				char (1)		null
	,CPL_CORR					int			not null
	,primary key (RUT,COD_INDICADOR,CPL_CORR)
)

	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #tabla_indicador02'
				RETURN -1
	END

	
	
Print 'Inserta calificaciones a tabla temporal #tabla_indicador'

/* ********** Variable: Calificacion Vencida ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,74
	,case 
        when (DATEDIFF(mm,a.CPL_FEC_CAL,CONVERT(SMALLDATETIME, CONVERT(CHAR(6), a.PERIODO_PROC) || '01'))) > (CONVERT(INT,b.PAR_ADICIONAL)) 
             then 'S' else 'N' end 
FROM
	#complemento_act a
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '74' 
    and b.PAR_VIGENCIA = 'S'


		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #complemento_act'
			return -1
		END


/* ********** Variable: PI ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,77
	,case when a.CPL_VAL_PI > (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end					
FROM
	#complemento_act a 
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '77' 
    and b.PAR_VIGENCIA = 'S'

	
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #complemento_act'
			return -1
		END
			

/* ********** Variable: marca prorroga, renegociado, refinanciamiento, reprogramacion ********** */
INSERT INTO #tabla_indicador02
SELECT 
	 a.RUT
	,120
	,case when a.CPL_MARCAR_ALR >= (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end	
	,CPL_CORR	
FROM
	#complemento_act_ope a 
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '120' 
    and b.PAR_VIGENCIA = 'S'

	
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #complemento_act'
			return -1
		END
			
			
/* **************************************** */
/*      		APR_HIST_DEUDASBIF 					*/
/* **************************************** */

Print 'Crea tabla temporal #deudasbif_act'
CREATE TABLE #deudasbif_act
(
	RUT							INT 	 		
	,PERIODO_PROC				DECIMAL(16,0)
	,SBF_MTO_MOR_30_90			DECIMAL(18,4)
	,SBF_MTO_DEU_VCD			DECIMAL(18,4)  
	,SBF_MTO_DEU_CAS			DECIMAL(18,4)  
	,SBF_MTO_DEU_VCD_IND		DECIMAL(18,4)  
	,SBF_MTO_DEU_CAS_IND		DECIMAL(18,4)  
	,SBF_MTO_DEU_MOR_LEA		DECIMAL(18,4)  
	,SBF_MTO_DEU_VCD_LEA		DECIMAL(18,4)  
	,SBF_MTO_DEU_DIR_SBIF		DECIMAL(18,4)  
    ,SBF_MTO_DEU_CAS_LEA		DECIMAL(18,4)
    ,SBF_MTO_IND_MOR_LEA        DECIMAL(18,4)
    ,SBF_MTO_IND_VCD_LEA        DECIMAL(18,4)
    ,SBF_MTO_IND_CAS_LEA        DECIMAL(18,4)
	,PRIMARY KEY (RUT,PERIODO_PROC)
)

	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #deudasbif_act'
				RETURN -1
	END

	

Print 'Insert a tabla #deudasbif_act datos de APR_HIST_DEUDASBIF'
INSERT INTO #deudasbif_act
SELECT 
     RUT
	,PERIODO_PROC
	,SBF_MTO_MOR_30_90		
	,SBF_MTO_DEU_VCD		
	,SBF_MTO_DEU_CAS		
	,SBF_MTO_DEU_VCD_IND	
	,SBF_MTO_DEU_CAS_IND	
	,SBF_MTO_DEU_MOR_LEA		
	,SBF_MTO_DEU_VCD_LEA	
	,SBF_MTO_DEU_DIR_SBIF
    ,SBF_MTO_DEU_CAS_LEA
    ,SBF_MTO_IND_MOR_LEA
    ,SBF_MTO_IND_VCD_LEA
    ,SBF_MTO_IND_CAS_LEA	
FROM APR_HIST_DEUDASBIF 	
WHERE 
	PERIODO_PROC = (SELECT convert(int,PAR_ADICIONAL) FROM APR_PARAMETRO where PAR_CODIGO = 'PERIODOACT' and PAR_VIGENCIA = 'S')


		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #deudasbif_act'
			return -1
		END		
		

/* ************************** CALCULOS *********************************** */	

/* ********** Variable: Deuda SBIF Mora 30 a 90 dias ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,42
    ,case when a.SBF_MTO_MOR_30_90 > (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end 
FROM
	#deudasbif_act a 
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '42' 
    and b.PAR_VIGENCIA = 'S'


		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #deudasbif_act'
			return -1
		END


/* ********** Variable: Deuda SBIF Directa Vencida ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,43
    ,case when a.SBF_MTO_DEU_VCD > (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end 
FROM
	#deudasbif_act a
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '43' 
    and b.PAR_VIGENCIA = 'S'
	
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #deudasbif_act'
			return -1
		END
	

/* ********** Variable: Deuda SBIF Directa Castigada ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,44
    ,case when a.SBF_MTO_DEU_CAS > (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end 
FROM
	#deudasbif_act a
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '44' 
    and b.PAR_VIGENCIA = 'S'

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #deudasbif_act'
			return -1
		END


/* ********** Variable: Deuda SBIF Indirecta Vencida ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,45
    ,case when a.SBF_MTO_DEU_VCD_IND > (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end 
FROM
	#deudasbif_act a
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '45' 
    and b.PAR_VIGENCIA = 'S'

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #deudasbif_act'
			return -1
		END

		
/* ********** Variable: Deuda SBIF Indirecta Castigada ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,46
    ,case when a.SBF_MTO_DEU_CAS_IND > (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end 
FROM
	#deudasbif_act a
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '46' 
    and b.PAR_VIGENCIA = 'S'

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #deudasbif_act'
			return -1
		END	
	
	
/* ********** Variable: Deuda Morosa Sistema Leasing (Achel) ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,56
    ,case when a.SBF_MTO_DEU_MOR_LEA > (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end 
FROM
	#deudasbif_act a
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '56' 
    and b.PAR_VIGENCIA = 'S'
	
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #deudasbif_act'
			return -1
		END	
	

/* ********** Variable: Deuda vencida sistema Leasing (Achel) ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,57
    ,case when a.SBF_MTO_DEU_VCD_LEA > (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end 
FROM
	#deudasbif_act a
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '57' 
    and b.PAR_VIGENCIA = 'S'
	
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #deudasbif_act'
			return -1
		END


/* ********** Variable: Deuda Castigada sistema Leasing (Achel) ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,80
    ,case when a.SBF_MTO_DEU_CAS_LEA > (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end 
FROM
	#deudasbif_act a
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '80' 
    and b.PAR_VIGENCIA = 'S'
	
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #deudasbif_act'
			return -1
		END


/* ********** Variable: Deuda Morosa Indirecta sistema Leasing (Achel) ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,81
    ,case when a.SBF_MTO_IND_MOR_LEA > (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end 
FROM
	#deudasbif_act a
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '81' 
    and b.PAR_VIGENCIA = 'S'
	
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #deudasbif_act'
			return -1
		END


/* ********** Variable: Deuda Vencida Indirecta sistema Leasing (Achel) ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,82
    ,case when a.SBF_MTO_IND_VCD_LEA > (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end 
FROM
	#deudasbif_act a
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '82' 
    and b.PAR_VIGENCIA = 'S'
	
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #deudasbif_act'
			return -1
		END
	

/* ********** Variable: Deuda Castigada Indirecta sistema Leasing (Achel) ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,83
    ,case when a.SBF_MTO_IND_CAS_LEA > (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end 
FROM
	#deudasbif_act a
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '83' 
    and b.PAR_VIGENCIA = 'S'
	
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #deudasbif_act'
			return -1
		END


/* **************************************** */
/*      		APR_HIST_PRTSBIF 					*/
/* **************************************** */

Print 'Crea tabla temporal #prtsbif_act'
CREATE TABLE #prtsbif_act
(
    PERIODO_PROC      decimal(16,0)
    ,RUT              int           
    ,PSB_CANT_PRT     int           
    ,SBF_MTO_PRT      decimal(18,4) 
    ,PSB_CANT_MOR_COM int           
    ,PSB_MTO_MOR_COM  decimal(18,4) 
    ,PSB_CANT_INF_LAB int           
    ,PRIMARY KEY (PERIODO_PROC,RUT)
)


	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #prtsbif_act'
				RETURN -1
	END

	

Print 'Insert a tabla #prtsbif_act datos de APR_HIST_PRTSBIF'
INSERT INTO #prtsbif_act
SELECT 
    PERIODO_PROC         
    ,RUT              
    ,PSB_CANT_PRT     
    ,SBF_MTO_PRT      
    ,PSB_CANT_MOR_COM 
    ,PSB_MTO_MOR_COM  
    ,PSB_CANT_INF_LAB 
FROM APR_HIST_PRTSBIF 	
WHERE 
	PERIODO_PROC = (SELECT convert(int,PAR_ADICIONAL) FROM APR_PARAMETRO where PAR_CODIGO = 'PERIODOACT' and PAR_VIGENCIA = 'S')


		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #prtsbif_act'
			return -1
		END
		
	
/* ************************** CALCULOS *********************************** */	

/* ********** Variable: Protestos del Sistema ********** */
INSERT INTO #tabla_indicador
select 
	a.RUT
	,58
	,case when a.PSB_CANT_PRT >= (CONVERT(INT,b.PAR_ADICIONAL)) 
            and a.SBF_MTO_PRT >= (CONVERT(INT,c.PAR_ADICIONAL)) 
                then 'S' else 'N' end 
from 
	#prtsbif_act a
    ,(SELECT PAR_ADICIONAL FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '58.1') b
    ,(SELECT PAR_ADICIONAL FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '58.2') c

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #prtsbif_act'
			return -1
		END

		
/* ********** Variable: Morosos del Comercio ********** */
INSERT INTO #tabla_indicador
select 
	a.RUT
	,59
	,case when a.PSB_CANT_MOR_COM > (CONVERT(INT,b.PAR_ADICIONAL)) 
        and a.PSB_MTO_MOR_COM > (CONVERT(INT,c.PAR_ADICIONAL)) 
            then 'S' else 'N' end 
from 
	#prtsbif_act a
    ,(SELECT PAR_ADICIONAL FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '59.1') b
    ,(SELECT PAR_ADICIONAL FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '59.2') c

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #prtsbif_act'
			return -1
		END	
	
	
/* ********** Variable: Infracciones Laborales ********** */
INSERT INTO #tabla_indicador
select 
	a.RUT	
	,60
	,case when a.PSB_CANT_INF_LAB > (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end 
from 
	#prtsbif_act a
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '60' 
    and b.PAR_VIGENCIA = 'S'

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #prtsbif_act'
			return -1
		END
		
		
/* **************************************** */
/*      		APR_HIST_PRTBCI    					*/
/* **************************************** */
	
Print 'Crea tabla temporal #prtbci_act'
CREATE TABLE #prtbci_act
(
    PERIODO_PROC         decimal(16,0)
    ,RUT                 int           
    ,PBI_CANT_PRT_FF     int           
    ,PBI_CANT_ONP        int           
    ,PBI_MTO_ONP         decimal(18,4) 
    ,PRIMARY KEY (PERIODO_PROC,RUT)
)


	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #prtbci_act'
				RETURN -1
	END


Print 'Insert a tabla #prtbci_act datos de APR_HIST_PRTBCI'	
INSERT INTO #prtbci_act
select 
	PERIODO_PROC        
	,RUT             
	,PBI_CANT_PRT_FF 
	,PBI_CANT_ONP    
	,PBI_MTO_ONP     
from 
	APR_HIST_PRTBCI
where
	PERIODO_PROC = (SELECT convert(int,PAR_ADICIONAL) FROM APR_PARAMETRO where PAR_CODIGO = 'PERIODOACT' and PAR_VIGENCIA = 'S')
	

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #prtbci_act'
			return -1
		END
		
	
/* ************************** CALCULOS *********************************** */	

/* ********** Variable: Cantidad de Protestos internos por Falta de Fondos ********** */
INSERT INTO #tabla_indicador
select 
	a.RUT	
	,61
	,case when PBI_CANT_PRT_FF >= (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end 
from 
	#prtbci_act a
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '61' 
    and b.PAR_VIGENCIA = 'S'

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #prtbci_act'
			return -1
		END


/* ********** Variable: Frecuencia de Ordenes de No Pago ********** */
INSERT INTO #tabla_indicador
select 
     a.RUT
	,63
	,case when a.PBI_CANT_ONP >= (CONVERT(INT,b.PAR_ADICIONAL)) and a.PBI_MTO_ONP >= (CONVERT(INT,c.PAR_ADICIONAL)) then 'S' else 'N' end 
from 
    #prtbci_act a
    ,(SELECT PAR_ADICIONAL FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '63.1') b
    ,(SELECT PAR_ADICIONAL FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '63.2') c


		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #prtbci_act'
			return -1
		END

		
/* **************************************** */
/*      		APR_HIST_DEUDABCI   					*/
/* **************************************** */
Print 'Crea tabla temporal #deudabci_act'
CREATE TABLE #deudabci_act
(
    PERIODO_PROC            decimal(16,0)
    ,RUT                    int           
    ,BCI_DEU_MOR_1_30       decimal(18,4) 
    ,BCI_DEU_MOR_30_90      decimal(18,4) 
    ,BCI_DEU_VCD_NAC        decimal(18,4) 
    ,BCI_DEU_VCD_INT        decimal(18,4) 
    ,BCI_DEU_CAS            decimal(18,4) 
    ,BCI_DEU_LEA_16_30      decimal(18,4) 
    ,BCI_DEU_LEA_30_MAS     decimal(18,4) 
    ,BCI_DEU_LEA_VCD        decimal(18,4) 
    ,BCI_FAC_MTO_MOR_1_30   decimal(18,4) 
    ,BCI_FAC_MTO_MOR_30_90  decimal(18,4) 
    ,BCI_FAC_MTO_MOR_90_MAS decimal(18,4) 
	,BCI_CANT_SGNP		   integer
	,BCI_MTO_MAX_SGNP	   decimal(18,4)
	,BCI_DIAS_SGNP		   integer
    ,BCI_IND_USO_SGNP_2MM  char(1)
	,PRIMARY KEY (PERIODO_PROC,RUT)
)


	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #deudabci_act'
				RETURN -1
	END


Print 'Insert a tabla #deudabci_act datos de APR_HIST_DEUDABCI'	
INSERT INTO #deudabci_act
select 
    PERIODO_PROC               
    ,RUT                    
    ,BCI_DEU_MOR_1_30       
    ,BCI_DEU_MOR_30_90      
    ,BCI_DEU_VCD_NAC        
    ,BCI_DEU_VCD_INT        
    ,BCI_DEU_CAS            
    ,BCI_DEU_LEA_16_30         
    ,BCI_DEU_LEA_30_MAS     
    ,BCI_DEU_LEA_VCD        
    ,BCI_FAC_MTO_MOR_1_30   
    ,BCI_FAC_MTO_MOR_30_90  
    ,BCI_FAC_MTO_MOR_90_MAS 
	,BCI_CANT_SGNP		   
	,BCI_MTO_MAX_SGNP	   
	,BCI_DIAS_SGNP
    ,BCI_IND_USO_SGNP_2MM		   
from 
	APR_HIST_DEUDABCI
where 
	PERIODO_PROC = (SELECT convert(int,PAR_ADICIONAL) FROM APR_PARAMETRO where PAR_CODIGO = 'PERIODOACT' and PAR_VIGENCIA = 'S')
	

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #deudabci_act'
			return -1
		END
		

/* ************************** CALCULOS *********************************** */	

/************** Variable: Mora Factoring tramo 0 a 30 dias ******************/
INSERT INTO #tabla_indicador
select 
     a.RUT 
	,64
    ,case when a.BCI_FAC_MTO_MOR_1_30 >= (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end
from 
	#deudabci_act a
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '64' 
    and b.PAR_VIGENCIA = 'S'

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #deudabci_act'
			return -1
		END

/************** Variable_ Mora Factoring tramo 30 a 90 dias*********************/
INSERT INTO #tabla_indicador
select 
    a.RUT 
	,65
	,case when a.BCI_FAC_MTO_MOR_30_90 >= (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end 
from 
	#deudabci_act a 
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '65' 
    and b.PAR_VIGENCIA = 'S'

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #deudabci_act'
			return -1
		END

/* ********** Variable: Mora Factoring tramo sobre 90 dias ********** */
INSERT INTO #tabla_indicador
select 
    a.RUT 
	,66
	,case when a.BCI_FAC_MTO_MOR_90_MAS >= (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end 
from 
	#deudabci_act a 
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '66' 
    and b.PAR_VIGENCIA = 'S'

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #deudabci_act'
			return -1
		END	

	

/* ********** Variable: Frecuencia de sobregiros no pactados ********** */
INSERT INTO #tabla_indicador
select 
    a.RUT 
	,62
	,case when a.BCI_CANT_SGNP >= (CONVERT(INT,b.PAR_ADICIONAL)) 
        and a.BCI_MTO_MAX_SGNP > (CONVERT(INT,c.PAR_ADICIONAL)) 
        and a.BCI_DIAS_SGNP > (CONVERT(INT,d.PAR_ADICIONAL))
            then 'S' else 'N' end
from 
	#deudabci_act a
    ,(SELECT PAR_ADICIONAL FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '62.1') b
    ,(SELECT PAR_ADICIONAL FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '62.2') c
    ,(SELECT PAR_ADICIONAL FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '62.3') d

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #deudabci_act'
			return -1
		END	

/* ********** Variable: Deuda BCI Castigada ********** */
INSERT INTO #tabla_indicador
select 
     a.RUT 
	,47
	,case when a.BCI_DEU_CAS > (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end 
from 
	#deudabci_act a
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '47' 
    and b.PAR_VIGENCIA = 'S'

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #deudabci_act'
			return -1
		END


/* ********** Variable: Deuda BCI Vencida ********** */
INSERT INTO #tabla_indicador
select 
    a.RUT 
	,48
	,case when a.BCI_DEU_VCD_NAC > (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end 
from 
	#deudabci_act a
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '48' 
    and b.PAR_VIGENCIA = 'S'

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #deudabci_act'
			return -1
		END


/* ********** Variable: Deuda BCI Moneda Extranjera Vencida ********** */
INSERT INTO #tabla_indicador
select 
    a.RUT 
	,49
	,case when a.BCI_DEU_VCD_INT > (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end 
from 
	#deudabci_act a
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '49' 
    and b.PAR_VIGENCIA = 'S'

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #deudabci_act'
			return -1
		END

		
/* ********** Variable: Deuda BCI Mora 0 a 30 dias ********** */
INSERT INTO #tabla_indicador
select 
    a.RUT 
	,50
	,case when a.BCI_DEU_MOR_1_30 > (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end 
from 
	#deudabci_act a 
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '50' 
    and b.PAR_VIGENCIA = 'S'

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #deudabci_act'
			return -1
		END
		
/* ********** Variable: Deuda BCI Mora 30 a 90 dias  ********** */
INSERT INTO #tabla_indicador
select 
    a.RUT 
	,52
	,case when a.BCI_DEU_MOR_30_90 > (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end 
from 
	#deudabci_act a
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '52' 
    and b.PAR_VIGENCIA = 'S'
	
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #deudabci_act'
			return -1
		END
	

/**************** Variable: Mora Leasing BCI 16 a 30 dias*************/
INSERT INTO #tabla_indicador
select 
    a.RUT 
	,53
	,case when a.BCI_DEU_LEA_16_30 > (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end
from 
	#deudabci_act a
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '53' 
    and b.PAR_VIGENCIA = 'S'

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #deudabci_act'
			return -1
		END


		
/* ********** Variable: Mora Leasing BCI Mayor a 30 dias ********** */
INSERT INTO #tabla_indicador
select 
    a.RUT 
	,54
	,case when a.BCI_DEU_LEA_30_MAS > (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end
from 
	#deudabci_act a
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '54' 
    and b.PAR_VIGENCIA = 'S'

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #deudabci_act'
			return -1
		END
	
	
/* ********** Variable: Deuda Leasing BCI Vencida ********** */
INSERT INTO #tabla_indicador
select 
    a.RUT 
	,55
	,case when a.BCI_DEU_LEA_VCD > (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end
from 
	#deudabci_act a
    ,APR_PARAMETRO b
where 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '55' 
    and b.PAR_VIGENCIA = 'S'

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #deudabci_act'
			return -1
		END
		

/* ********** Variable: Frecuencia de sobregiros no pactados ********** */
INSERT INTO #tabla_indicador
select 
     RUT 
	,79
	,BCI_IND_USO_SGNP_2MM
from 
	#deudabci_act

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #deudabci_act'
			return -1
		END
		
		
/* **************************************** */
/*      		APR_BALANCE   					*/
/* **************************************** */
Print 'Crea tabla temporal #balance_act'
CREATE TABLE #balance_act
(
	 RUT			 integer
	,BIF_IND_PER	 char(3)
	,BIF_TIP_BAL	 smallint
	,BIF_FEC_BAL	 datetime
    ,BIF_MTO_ROP     decimal(18,6) 
    ,BIF_MTO_PAT     decimal(18,6) 
	,BIF_MTO_UTL	 decimal(18,6)
    ,BIF_MTO_CEXP    decimal(18,6) 
    ,BIF_MTO_EBITDA  decimal(18,6) 
    ,BIF_PER_CTA_CBR integer
    ,BIF_CTA_CBR     decimal(18,6) 
    ,BIF_PER_INV     integer 
    ,BIF_IND_LQC     decimal(18,6)  
    ,BIF_DEU_FIN     decimal(18,6)
    ,BIF_GTO_FIN     decimal(18,6) 
    ,BIF_PAS_EXG     decimal(18,6) 
    ,BIF_TOT_ACT     decimal(18,6) 
    ,BIF_VTA_ANU     decimal(18,6)
    ,BIF_MTO_ITG     decimal(18,6) 
    ,BIF_CTA_CBR_ER  decimal(18,6) 
    ,BIF_INV_ER      decimal(18,6) 
    ,BIF_IND_LVG_FIN      decimal(18,6) 
	,PRIMARY KEY (RUT,BIF_IND_PER,BIF_TIP_BAL,BIF_FEC_BAL)
)


	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #balance_act'
				RETURN -1
	END


Print 'Insert a tabla #balance_act datos de APR_BALANCE'	
INSERT INTO #balance_act
SELECT 
	 RUT
	,BIF_IND_PER	 
	,BIF_TIP_BAL	 
	,BIF_FEC_BAL	 
	,convert(decimal(18,4),(BIF_MTO_ROP / (convert(int,(substring(( convert(varchar, BIF_FEC_BAL, 112)),5,2))))) * 12) as BIF_MTO_ROP	
    ,BIF_MTO_PAT     	
    ,convert(decimal(18,4),(BIF_MTO_UTL / (convert(int,(substring(( convert(varchar, BIF_FEC_BAL, 112)),5,2))))) * 12) as BIF_MTO_UTL 
    ,BIF_MTO_CEXP  	 
    ,convert(decimal(18,4),(BIF_MTO_EBITDA / (convert(int,(substring(( convert(varchar, BIF_FEC_BAL, 112)),5,2))))) * 12) as BIF_MTO_EBITDA
    ,BIF_PER_CTA_CBR  
    ,BIF_CTA_CBR     
    ,BIF_PER_INV     
    ,BIF_IND_LQC
    ,convert(decimal(18,4),(BIF_DEU_FIN / (convert(int,(substring(( convert(varchar, BIF_FEC_BAL, 112)),5,2))))) * 12) as BIF_DEU_FIN	
    ,BIF_GTO_FIN     
    ,BIF_PAS_EXG    
    ,BIF_TOT_ACT     
    ,convert(decimal(18,4),(BIF_VTA_ANU / (convert(int,(substring(( convert(varchar, BIF_FEC_BAL, 112)),5,2))))) * 12) as BIF_VTA_ANU	 
    ,BIF_MTO_ITG     
    ,BIF_CTA_CBR_ER  
    ,BIF_INV_ER 
	,BIF_IND_LVG_FIN	
FROM 
	APR_BALANCE
WHERE 
    BIF_IND_PER = 'ACT'
	
	

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #balance_act'
			return -1
		END	
	

/* ************************** CALCULOS *********************************** */	

/* ********** Variable: Deuda SBIF Directa ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,38 
	,case when (a.SBF_MTO_DEU_DIR_SBIF * (CONVERT(DECIMAL(4,2),c.PAR_ADICIONAL)) > b.BIF_MTO_ROP * (CONVERT(DECIMAL(4,2),d.PAR_ADICIONAL)) ) 
			then 'S' else 'N' end 
FROM
	 #deudasbif_act a
	 ,#balance_act b
     ,(SELECT PAR_ADICIONAL FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '38.1') c
     ,(SELECT PAR_ADICIONAL FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '38.2') d
WHERE 
	a.RUT = b.RUT

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #balance_act'
			return -1
		END


/* ********** Variable: Deuda SBIF Directa ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,39 
    ,case when a.BIF_MTO_EBITDA<>0 then  
            case when (b.SBF_MTO_DEU_DIR_SBIF / a.BIF_MTO_EBITDA) > (CONVERT(INT,c.PAR_ADICIONAL)) then 'S' else 'N' end 
          else 'N' 
     end    
FROM
	 #balance_act a
	,#deudasbif_act b
    ,APR_PARAMETRO c
WHERE 
	a.RUT = b.RUT
    and c.PAR_CONCEPTO = 'INDICADORES' 
    and c.PAR_CODIGO = '39' 
    and c.PAR_VIGENCIA = 'S'    


		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #balance_act'
			return -1
		END


 /* ********** Variable: Liquidez Corriente ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,25
	,case when a.BIF_IND_LQC < (CONVERT(DECIMAL(4,2),b.PAR_ADICIONAL)) then 'S' else 'N' end
FROM
	 #balance_act a
    ,APR_PARAMETRO b
WHERE 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '25' 
    and b.PAR_VIGENCIA = 'S'

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #balance_act'
			return -1
		END	 

		
/* ********** Variable: Liquidez Corriente ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,113
	,case when a.BIF_IND_LQC < (CONVERT(DECIMAL(4,2),b.PAR_ADICIONAL)) then 'S' else 'N' end
FROM
	 #balance_act a
    ,APR_PARAMETRO b
WHERE 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '113' 
    and b.PAR_VIGENCIA = 'S'

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #balance_act'
			return -1
		END	 
		

/* ********** Variable: Deuda Financiera sobre Ebitda ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,26
    ,case when a.BIF_MTO_EBITDA <>0 then  
            case when (a.BIF_DEU_FIN / a.BIF_MTO_EBITDA) > (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) then 'S' else 'N' end 
          else 'N' 
     end
    
FROM
	 #balance_act a
     ,APR_PARAMETRO b 
WHERE
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '26' 
    and b.PAR_VIGENCIA = 'S'    

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #Fin_Ebitda desde #balance_act'
			return -1
		END
		

		
Print 'Crea tabla temporal #Fin_Ebitda'
CREATE TABLE #Fin_Ebitda
(
	 RUT				int 	 		not null		
	,COD_INDICADOR  	smallint    	not null
	,ESTADO				char (1)		null
	,primary key (RUT,COD_INDICADOR)
)


/* ********** Variable: Deuda Financiera sobre Ebitda ********** */
INSERT INTO #Fin_Ebitda
SELECT 
	 a.RUT
	,27
    ,case when a.BIF_MTO_EBITDA <> 0 then  
            case when (a.BIF_DEU_FIN / a.BIF_MTO_EBITDA) > (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) then 'S' else 'N' end 
          else 'N' 
     end    
    
FROM
	 #balance_act a
     ,APR_PARAMETRO b 
WHERE
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '27' 
    and b.PAR_VIGENCIA = 'S'

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #Fin_Ebitda desde #balance_act'
			return -1
		END

/* Deja la alerta mas grave, en caso que el alerta mas debil este contenida en la mas grave */
INSERT INTO #tabla_indicador
SELECT  
     RUT
    ,max(COD_INDICADOR)
    ,ESTADO 
FROM 
    #Fin_Ebitda
WHERE 
    ESTADO = 'S'
GROUP BY RUT,ESTADO

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #Fin_Ebitda desde #balance_act'
			return -1
		END		


/* ********** Variable: Gastos Financieros sobre Ebitda ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,28
    ,case when a.BIF_MTO_EBITDA <> 0 then  
            case when (a.BIF_GTO_FIN / a.BIF_MTO_EBITDA) > (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) then 'S' else 'N' end 
          else 'N' 
     end     

FROM
	 #balance_act a
     ,APR_PARAMETRO b 
WHERE
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '28' 
    and b.PAR_VIGENCIA = 'S'

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #balance_act'
			return -1
		END 



 /* ********** Variable: Permanencia Inventario (dias) ********** */

INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,16
	,case when a.BIF_PER_INV >= (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end
FROM
	 #balance_act a
    ,APR_PARAMETRO b
WHERE 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '16' 
    and b.PAR_VIGENCIA = 'S'       
	
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #balance_act'
			return -1
		END


		 /* ********** Variable: Permanencia Inventario (dias) ********** */

INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,92
	,case when a.BIF_PER_INV >= (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end
FROM
	 #balance_act a
    ,APR_PARAMETRO b
WHERE 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '92' 
    and b.PAR_VIGENCIA = 'S'       
	
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #balance_act'
			return -1
		END

		
/* ********** Variable: Permanencia Inventario ( montos) ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,98
	,case when a.BIF_PER_INV > (a.BIF_TOT_ACT * (CONVERT(DECIMAL(4,2),b.PAR_ADICIONAL))) then 'S' else 'N' end
FROM
	 #balance_act a
    ,APR_PARAMETRO b
WHERE 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '98' 
    and b.PAR_VIGENCIA = 'S'       

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #balance_act'
			return -1
		END	 		
		
		
		/* ********** Variable: Intangibles ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,32
	,case when a.BIF_MTO_ITG > (a.BIF_TOT_ACT * (CONVERT(DECIMAL(4,2),b.PAR_ADICIONAL))) then 'S' else 'N' end
FROM
	 #balance_act a
    ,APR_PARAMETRO b
WHERE 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '32' 
    and b.PAR_VIGENCIA = 'S'       

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #balance_act'
			return -1
		END	 

/* ********** Variable: Rop negativo ultimo periodo ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,1
	,CASE 	WHEN a.BIF_MTO_ROP IS NULL THEN 'N'	
			WHEN a.BIF_MTO_ROP < (CONVERT(INT,PAR_ADICIONAL)) THEN 'S' ELSE 'N' END  
FROM 
	#balance_act a
    ,APR_PARAMETRO b
WHERE 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '1' 
    and b.PAR_VIGENCIA = 'S'     



		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Rop negativo último período'
			return -1
		END

/* ********** Variable: Patrimonio negativo ultimo periodo ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,4
	,CASE 	WHEN a.BIF_MTO_PAT IS NULL THEN 'N'	
			WHEN a.BIF_MTO_PAT < (CONVERT(INT,b.PAR_ADICIONAL)) THEN 'S' ELSE 'N' END  
FROM 
	#balance_act a
    ,APR_PARAMETRO b
WHERE 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '4' 
    and b.PAR_VIGENCIA = 'S'   


		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Rop negativo ultimo periodo'
			return -1
		END

		
/* ************** Variable: Utilidad Perdida ultimo periodo********************* */		

INSERT INTO #tabla_indicador
SELECT 
	 RUT
	,6
	,CASE 	WHEN BIF_MTO_UTL IS NULL THEN 'N'	
			WHEN BIF_MTO_UTL < (CONVERT(INT,b.PAR_ADICIONAL)) THEN 'S' ELSE 'N' END 
FROM 
	 #balance_act 
    ,APR_PARAMETRO b
WHERE 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '6' 
    and b.PAR_VIGENCIA = 'S' 

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Utilidad, Perdida ultimo periodo '
			return -1
		END


		
		
/* ********** Variable: Ebitda ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,9
	,CASE   WHEN a.BIF_MTO_EBITDA IS NULL THEN 'N'	
			    WHEN a.BIF_MTO_EBITDA < (CONVERT(INT,b.PAR_ADICIONAL)) 
                    THEN 'S' ELSE 'N' END 
FROM 
	 #balance_act a
    ,APR_PARAMETRO b
WHERE 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '9' 
    and b.PAR_VIGENCIA = 'S' 


		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicado, Variable: Ebitda'
			return -1
		END	 
	
	
/* ********** Variable: Permanencia Inventario ( montos): NUEVA (combinacion apoyo  indicadores 16 y 98) ********** */
INSERT INTO #tabla_indicador
select 
    U.RUT
    ,99
    ,CASE WHEN A.RUT IS NOT NULL AND B.RUT IS NOT NULL THEN 'S'
        ELSE 'N' END
from
    #balance_act U
LEFT JOIN
    #tabla_indicador A
ON U.RUT = A.RUT
AND A.COD_INDICADOR = 16
AND A.ESTADO = 'S'
LEFT JOIN
    #tabla_indicador B
ON U.RUT = B.RUT
AND B.COD_INDICADOR = 98
AND B.ESTADO = 'S'

	
			IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicado, Variable: Permanencia Inventario ( montos): NUEVA'
			return -1
		END	 

/* ********** Variable: Liquidez Corriente : NUEVA (combinacion apoyo  indicadores 25 y 26) ********** */
INSERT INTO #tabla_indicador
select 
    U.RUT
    ,104
    ,CASE WHEN A.RUT IS NOT NULL AND B.RUT IS NOT NULL THEN 'S'
        ELSE 'N' END
from
    #balance_act U
LEFT JOIN
    #tabla_indicador A
ON U.RUT = A.RUT
AND A.COD_INDICADOR = 25
AND A.ESTADO = 'S'
LEFT JOIN
    #tabla_indicador B
ON U.RUT = B.RUT
AND B.COD_INDICADOR = 26
AND B.ESTADO = 'S'

	
			IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicado, Variable: Liquidez Corriente: NUEVA'
			return -1
		END	 

		
/* ********** Variable: Deuda Financiera ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,112
    ,CASE 	WHEN b.RUT IS NULL THEN 'N'	
				WHEN b.CPL_MTO_VTA_MES = 0 THEN 'N'
				WHEN a.BIF_DEU_FIN > (b.CPL_MTO_VTA_MES / ((SELECT (CONVERT(decimal(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '112')) ) 
                    THEN 'S' ELSE 'N' END
FROM
	 #balance_act a
LEFT JOIN
	 #complemento_act b
ON
	a.RUT = b.RUT
	
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Deuda Financiera '
			return -1
		END			

		
/* ************************************ */		
/*		tabla temp de salida			*/
/* ************************************ */		
		
Print 'Crea tabla temporal #tab_salida'
CREATE TABLE #tab_salida
(
	 RUT				int 	 		not null		
	,CODIGO				varchar(10)		not null
	,PERIODO_PROC		decimal(16,0)	null
	,PAR_CODIGO			char(20)		null
	,COD_INDICADOR		smallint		null
	,FEC_EJEC			datetime		null
	,CPL_CORR			int				not null
	,primary key (RUT,CODIGO,CPL_CORR)
)


	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #tab_salida'
				RETURN -1
	END


	
print 'inserta registros a la tabla #tab_salida'	
INSERT INTO #tab_salida
select 
    a.RUT
    ,a.PAR_CODIGO + convert(varchar,b.COD_INDICADOR)
    ,a.PERIODO_PROC
    ,a.PAR_CODIGO 
    ,b.COD_INDICADOR
	,a.FEC_EJEC
	,1
from 
    #banca_cliente a
    ,#tabla_indicador b
WHERE 
    a.RUT = b.RUT
and b.ESTADO = 'S'


		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #tab_salida'
			return -1
		END	 



print 'inserta registros a la tabla #tab_salida'	
INSERT INTO #tab_salida
select 
    a.RUT
    ,a.PAR_CODIGO + convert(varchar,b.COD_INDICADOR)
    ,a.PERIODO_PROC
    ,a.PAR_CODIGO 
    ,b.COD_INDICADOR
	,a.FEC_EJEC
	,b.CPL_CORR
from 
    #banca_cliente a
    ,#tabla_indicador02 b
WHERE 
    a.RUT = b.RUT
and b.ESTADO = 'S'


		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #tab_salida'
			return -1
		END	 

		
print 'inserta registros a la tabla APR_HIST_INDICADOR'	
INSERT INTO APR_HIST_INDICADOR		
SELECT 
     a.PERIODO_PROC
    ,a.RUT
    ,a.COD_INDICADOR
    ,b.PAR_ADICIONAL
    ,a.FEC_EJEC
	,a.CPL_CORR
FROM
    #tab_salida a
left join
    APR_PARAMETRO b
on
    a.CODIGO = b.PAR_CODIGO
WHERE 
    b.PAR_CONCEPTO = 'CLASALERTAS'    
AND b.PAR_VIGENCIA = 'S'		
AND b.PAR_ADICIONAL <> 'N/A'


		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla APR_HIST_INDICADOR'
			return -1
		END

END

drop table #banca_cliente
drop table #tabla_indicador
drop table #complemento_act
drop table #deudasbif_act
drop table #prtsbif_act
drop table #prtbci_act
drop table #balance_act
drop table #tab_salida
drop table #Fin_Ebitda
drop table #deudabci_act
drop table #complemento_act_ope
drop table #tabla_indicador02


go
IF OBJECT_ID('sp_alerta_calc_act') IS NOT NULL
    PRINT '<<< CREATED PROCEDURE sp_alerta_calc_act >>>'
ELSE
    PRINT '<<< FAILED CREATING PROCEDURE sp_alerta_calc_act >>>'
go
EXEC sp_procxmode 'sp_alerta_calc_act','unchained'
go
GRANT EXECUTE ON sp_alerta_calc_act TO ejecucion
go
GRANT EXECUTE ON sp_alerta_calc_act TO mantencion
go