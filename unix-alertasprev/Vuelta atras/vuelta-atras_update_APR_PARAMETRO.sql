use alertasprev
go

UPDATE APR_PARAMETRO
set PAR_DESCRIPCION = 'Infracciones > 3 por no pago previsional, sin incl'
	,PAR_ADICIONAL = '3'
WHERE 
PAR_CODIGO = '60'
go

UPDATE APR_PARAMETRO
SET PAR_ADICIONAL= 'A'
WHERE 
PAR_CODIGO IN ('EE60                ' , 'GGEE60              ')
go
