.logon usuario,password;
SEL DATE, TIME;


/* **************************************************************************************************** */
/*																										*/
/*Nombre Bteq				:50_APR_CLI_Salida_Datos_SBIF.sql											*/
/*Nombre BD					:EDW_VW																		*/
/*Tipo de ejecucion			:Batch																		*/
/*Fecha de creacion			:05/03/2016																	*/
/*Autor						:Enrique S. --ADA LTDA														*/
/*Objetivos					:Se relaciona la informacion de cliente obtenido en procesos anteriores, 	*/
/*							 con la informacion de protestos del sistema, infracciones laborales 		*/
/*Canal de ejecucion		:Control-M																	*/
/*Parametros de entrada		:${BD_VW}																	*/
/*							 ${BD_TEMP}																	*/
/*Retorno					:Clientes con informacion de protestos del sistema, infracciones laborales  */
/*							 y morosos del comercio														*/
/*Ejemplo de ejecucion		:No aplica, ya que es ejecutado por medio de una shell						*/
/* **************************************************************************************************** */
/*																										*/
/*Fecha ultima modificacion :01/09/2021																	*/
/*Autor modificacion		:Francisco V. --ADA LTDA													*/
/*Objetivo modificacion		:Clientes con los nuevos campos obtenido en el proceso anterior de protesto */
/*							 (monto y cantidad), mora comercio (monto, cantidad, fecha) e infracciones  */
/*							 laborales (monto, meses deuda)												*/
/* **************************************************************************************************** */


/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
/*						TABLA RESUMEN DE PROTESTOS								*/
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
DROP TABLE  ${BD_TEMP}.APR_CLI_PROTESTOS_SBF_F;  
CREATE TABLE ${BD_TEMP}.APR_CLI_PROTESTOS_SBF_F
(
	 FEC_PROC					DATE
	,PARTY_ID					INTEGER
	,RUT						INTEGER
	,PSB_CANT_PRT_FF			INTEGER
	,PSB_MTO_PRT_FF				DECIMAL (18,4)
	,PSB_CANT_MOR_COM			INTEGER
	,PSB_MTO_MOR_COM			DECIMAL (18,4)
	,PSB_MAX_FEC_MOR_COM		DATE
	,PSB_CANT_INF_LAB			INTEGER
	,PSB_MTO_INF_LAB			DECIMAL(18,4)
	,PSB_MAX_MESES_DDA_LAB 		INTEGER
	,FEC_EJEC					DATE
)
UNIQUE PRIMARY INDEX(RUT);

.IF ERRORCODE<>0 THEN .QUIT 8;


INSERT INTO ${BD_TEMP}.APR_CLI_PROTESTOS_SBF_F
SELECT
	 U.DIAULTMES
	,U.PARTY_ID
	,U.RUT
	,A.PSB_CANT_VIG_MES
	,A.PSB_MTO_PRT_VIG_MES
	,A.PSB_CANT_MOR_COM
	,A.PSB_MTO_MOR_COM
	,A.PSB_MAX_FEC_MOR_COM
	,A.PSB_CANT_INF_LAB
	,A.PSB_MTO_INF_LAB
	,A.PSB_MAX_MESES_DDA_LAB
	,${BD_TEMP}.APR_CLI_FECHA_PROC_I.FECHA_HOY
FROM
	${BD_TEMP}.APR_CLI_UNI_CLI_S			U
LEFT JOIN
	${BD_TEMP}.APR_CLI_DATOS_SBIF_S 		A
ON  U.RUT=A.RUT
;

.IF ERRORCODE<>0 THEN .QUIT 8;

COLLECT STATISTICS USING SAMPLE
INDEX ( RUT)
ON ${BD_TEMP}.APR_CLI_PROTESTOS_SBF_F;
.IF ERRORCODE<>0 THEN .QUIT 8;


/*----------------------------------------------------------------------------------------------*/
/*			Elimina Tabla																		*/
/*----------------------------------------------------------------------------------------------*/
DROP TABLE ${BD_TEMP}.APR_CLI_DATOS_SBIF_S;



SEL DATE, TIME;
.LOGOFF
.QUIT 0;

