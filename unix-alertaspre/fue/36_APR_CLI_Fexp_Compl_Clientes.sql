.logon usuario,password;

/* **************************************************************************************************** */
/*																										*/
/*Nombre Bteq				:36_APR_CLI_Fexp_Compl_Clientes.sql											*/
/*Nombre BD					:EDW_VW																		*/
/*Tipo de ejecucion			:Batch																		*/
/*Fecha de creacion			:05/03/2016																	*/
/*Autor						:Enrique S. --ADA LTDA														*/
/*Objetivos					:Se exporta todas los eventos de operaciones de colocacion, en las cuales 	*/
/*							incluye las operaciones de deuda directa, obteniendo los tipos de pagos 	*/
/*							para identificar prorrogas y renegociados, a un archivo plano				*/
/*Canal de ejecucion		:Control-M																	*/
/*Parametros de entrada		:${BD_VW}																	*/
/*							 ${BD_TEMP}																	*/
/*Retorno					:Archivo plano con las operaciones prorrogadas y renegociadas				*/
/*Ejemplo de ejecucion		:No aplica, ya que es ejecutado por medio de una shell						*/
/* **************************************************************************************************** */
/*																										*/
/*Fecha ultima modificacion :01/09/2021																	*/
/*Autor modificacion		:Francisco V. --ADA LTDA													*/
/*Objetivo modificacion		:Se agregan al archivo de salida los nuevos campos obtenido en el proceso 	*/
/*							 anterior (numero de colocacion, montos y fechas para las operaciones)   	*/
/*							 prorrogadas y renegociadas)												*/
/* **************************************************************************************************** */



.LOGTABLE ${BD_TEMP}.APR_CLI_COMPLEM_EXPORT;
.BEGIN EXPORT;
.EXPORT OUTFILE $ARCH_COM_CLI
MODE RECORD FORMAT TEXT;
SELECT
(
	(CAST((FEC_PROC							(FORMAT 'YYYYMM'))			AS VARCHAR(10)))	|| ';'	||
	(CAST((RUT								(FORMAT '--Z(18)9'))		AS VARCHAR(10)))	|| ';'	||	
	(CAST((CPL_MTO_LCG_APR					(FORMAT '--Z(18)9'))		AS VARCHAR(15)))	|| ';'	||	
	(CAST((CPL_FEC_VEN_LCG					(FORMAT 'YYYY-MM-DD'))		AS VARCHAR(10)))	|| ';'	||
	(CAST(CPL_COD_CLA_BCI												AS VARCHAR(3))) 	|| ';'	||	
	(CAST((CPL_POR_SOW						(FORMAT '--Z(18)9'))		AS VARCHAR(15)))	|| ';'	||	
	(CAST((CPL_FEC_ULT_APC_RES				(FORMAT 'YYYY-MM-DD'))		AS VARCHAR(10)))	|| ';'	||
	(CAST(CPL_EST_ULT_APC_RES											AS VARCHAR(20)))	|| ';'	||	
	(CAST((CPL_FEC_ULT_APC					(FORMAT 'YYYY-MM-DD'))		AS VARCHAR(10)))	|| ';'	||
	(CAST(CPL_EST_ULT_APC												AS VARCHAR(20)))	|| ';'	||	
	(CAST((CPL_FEC_ULT_ETG_EPY				(FORMAT 'YYYY-MM-DD'))		AS VARCHAR(10)))	|| ';'	||
	(CAST(CPL_EST_ULT_ETG_EPY											AS VARCHAR(20)))	|| ';'	||	
	(CAST(CPL_IND_CLI_COS_EPY											AS VARCHAR(20)))	|| ';'	||	
	(CAST((CPL_FEC_CAL						(FORMAT 'YYYY-MM-DD'))		AS VARCHAR(10)))	|| ';'	||
	(CAST((CPL_VAL_PI						(FORMAT '--Z(18)9.9(6)'))	AS VARCHAR(10)))	|| ';'	||	
	(CAST(CPL_MOD														AS VARCHAR(25)))	|| ';'	||	
	(CAST((CPL_SDO_PROM_MES					(FORMAT '--Z(18)9'))		AS VARCHAR(15)))	|| ';'	||	
	(CAST((CPL_MTO_VTA_MES					(FORMAT '--Z(18)9'))		AS VARCHAR(15)))	|| ';'	||	
	(CAST((CPL_VAL_LIQ_GAR_GEN				(FORMAT '--Z(18)9'))		AS VARCHAR(15)))	|| ';'	||	
	(CAST((CPL_VAL_LIQ_GAR_ESP				(FORMAT '--Z(18)9'))		AS VARCHAR(15)))	|| ';'	||	
	(CAST((CPL_CANT_PRG_TOT					(FORMAT '--Z(18)9'))		AS VARCHAR(10)))	|| ';'	||	
	(CAST((CPL_CANT_REN						(FORMAT '--Z(18)9'))		AS VARCHAR(10)))	|| ';'	||
	(CAST((FEC_EJEC							(FORMAT 'YYYY-MM-DD'))		AS VARCHAR(10)))	|| ';'	||
	(CAST((BCI_PROCESS_DATE					(FORMAT 'YYYY-MM-DD'))		AS VARCHAR(10)))	|| ';'	||	
	(CAST((EVENT_START_DT					(FORMAT 'YYYY-MM-DD'))		AS VARCHAR(10)))	|| ';'	||
	(CAST(ACCOUNT_NUM													AS VARCHAR(18)))	|| ';'	||
	(CAST(ACCOUNT_MODIFIER_NUM											AS VARCHAR(18)))	|| ';'	||
	(CAST((BCI_AMT1							(FORMAT '--Z(18)9'))		AS VARCHAR(10)))	|| ';'	||
	(CAST((BCI_AMT2							(FORMAT '--Z(18)9'))		AS VARCHAR(10)))	|| ';'	||
	(CAST((FEC_ING_BAL						(FORMAT 'YYYY-MM-DD'))		AS VARCHAR(10)))	|| ';'	||
	(CAST(OPE_COP_PRO_REN												AS VARCHAR(18)))	|| ';'	||
	(CAST((NROS_PRO_REN						(FORMAT '--Z(18)9'))		AS VARCHAR(10)))	|| ';'	||
	(CAST(MARCA_PRO														AS VARCHAR(5)))		|| ';'	||
	(CAST((MARCAR_ALR						(FORMAT '--Z(18)9'))		AS VARCHAR(10)))	|| ';'	||
	(CAST((CPL_CORR							(FORMAT '--Z(18)9'))		AS VARCHAR(10)))	|| ''
	)
	(CHAR (570))(TITLE '')
 FROM 
	${BD_TEMP}.APR_CLI_DATOS_COMPLEMENT_F

;
.END EXPORT;

SEL DATE, TIME;

.LOGOFF;
.QUIT 0;
