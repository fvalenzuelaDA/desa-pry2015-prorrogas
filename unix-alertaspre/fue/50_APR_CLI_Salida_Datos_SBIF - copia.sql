.logon 161.131.4.10/dconfsbp,dbnpp001;
SEL DATE, TIME;

/*----------------------------------------------------------------------------------------------------------------------------
TABLAS Entrada:

TABLAS Salida:


----------------------------------------------------------------------------------------------------------------------------*/



/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*		PROTESTOS SBIF						*/
/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*		MOROSOS COMERCIO						*/
/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*		INFRACCIONES LABORALES						*/
/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/




/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
/*						TABLA RESUMEN DE PROTESTOS								*/
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
DROP TABLE  EDW_TEMP.APR_CLI_PROTESTOS_SBF_F;  
CREATE MULTISET TABLE EDW_TEMP.APR_CLI_PROTESTOS_SBF_F
(
	 FEC_PROC					DATE
	,PARTY_ID					INTEGER
	,RUT						INTEGER
	,PSB_CANT_PRT_FF			INTEGER
	,PSB_MTO_PRT_FF				DECIMAL (18,4)
	,PSB_CANT_MOR_COM			INTEGER
	,PSB_MTO_MOR_COM			DECIMAL (18,4)
	,PSB_MAX_FEC_MOR_COM		DATE
	,PSB_CANT_INF_LAB			INTEGER
	,PSB_MTO_INF_LAB			DECIMAL(18,4)
	,PSB_MAX_MESES_DDA_LAB 		INTEGER
	,FEC_EJEC					DATE
)
UNIQUE PRIMARY INDEX(RUT);

.IF ERRORCODE<>0 THEN .QUIT 8;


INSERT INTO EDW_TEMP.APR_CLI_PROTESTOS_SBF_F
SELECT
	 U.DIAULTMES
	,U.PARTY_ID
	,U.RUT
	,A.PSB_CANT_VIG_MES
	,A.PSB_MTO_PRT_VIG_MES
	,A.PSB_CANT_MOR_COM
	,A.PSB_MTO_MOR_COM
	,A.PSB_MAX_FEC_MOR_COM
	,A.PSB_CANT_INF_LAB
	,A.PSB_MTO_INF_LAB
	,A.PSB_MAX_MESES_DDA_LAB
	,EDW_TEMP.APR_CLI_FECHA_PROC_I.FECHA_HOY
FROM
	EDW_TEMP.APR_CLI_UNI_CLI_S			U
LEFT JOIN
	EDW_TEMP.APR_CLI_DATOS_SBIF_S 		A
ON  U.RUT=A.RUT
;

.IF ERRORCODE<>0 THEN .QUIT 8;

COLLECT STATISTICS USING SAMPLE
INDEX ( RUT)
ON edw_temp.APR_CLI_PROTESTOS_SBF_F;
.IF ERRORCODE<>0 THEN .QUIT 8;


/*----------------------------------------------------------------------------------------------*/
/*			Elimina Tabla																		*/
/*----------------------------------------------------------------------------------------------*/
--DROP TABLE EDW_TEMP.APR_CLI_DATOS_SBIF_S;



SEL DATE, TIME;
.LOGOFF
.QUIT 0;

