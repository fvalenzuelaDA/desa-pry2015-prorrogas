.logon usuario,password;

SEL DATE, TIME;


/* **************************************************************************************************** */
/*																										*/
/*Nombre Bteq				:35_APR_CLI_Salida_Compl_Clientes.sql										*/
/*Nombre BD					:EDW_VW																		*/
/*Tipo de ejecucion			:Batch																		*/
/*Fecha de creacion			:05/03/2016																	*/
/*Autor						:Enrique S. --ADA LTDA														*/
/*Objetivos					:Se relaciona la informacion de cliente obtenido en procesos anteriores,    */
/*							 con la informacion de colocaciones prorrogadas o renegociadas				*/
/*Canal de ejecucion		:Control-M																	*/
/*Parametros de entrada		:${BD_VW}																	*/
/*							 ${BD_TEMP}																	*/
/*Retorno					:Clientes con operaciones prorrogadas y renegociadas							*/
/*Ejemplo de ejecucion		:No aplica, ya que es ejecutado por medio de una shell						*/
/* **************************************************************************************************** */
/*																										*/
/*Fecha ultima modificacion :01/09/2021																	*/
/*Autor modificacion		:Francisco V. --ADA LTDA													*/
/*Objetivo modificacion		:Clientes con los nuevos campos obtenido en el proceso anterior (numero de */
/*							 colocacion, montos y fechas para las operaciones)   						*/
/*							 prorrogadas y renegociadas)												*/
/* **************************************************************************************************** */


/*--------------------------------------------------------------------------------------------------------*/
/*		TABLA FINAL CON DATOS COMPLEMENTARIOS DE CLIENTES  */
/*--------------------------------------------------------------------------------------------------------*/

DROP TABLE  ${BD_TEMP}.APR_CLI_DATOS_COMPLEMENT_F ;  
CREATE SET TABLE ${BD_TEMP}.APR_CLI_DATOS_COMPLEMENT_F 
(
	 FEC_PROC					DATE
	,RUT						INTEGER
	,CPL_MTO_LCG_APR			DECIMAL (18,4)
	,CPL_FEC_VEN_LCG			DATE
	,CPL_COD_CLA_BCI			VARCHAR(3)
	,CPL_POR_SOW				DECIMAL (18,4)
	,CPL_FEC_ULT_APC_RES		DATE
	,CPL_EST_ULT_APC_RES		VARCHAR (20)
	,CPL_FEC_ULT_APC			DATE
	,CPL_EST_ULT_APC			VARCHAR (20)
	,CPL_FEC_ULT_ETG_EPY		DATE
	,CPL_EST_ULT_ETG_EPY		VARCHAR (20)
	,CPL_IND_CLI_COS_EPY		VARCHAR (20)
	,CPL_FEC_CAL				DATE
	,CPL_VAL_PI					DECIMAL (22,8)
	,CPL_MOD					VARCHAR (25)
	,CPL_SDO_PROM_MES			DECIMAL (18,4)
	,CPL_MTO_VTA_MES			DECIMAL (18,4)
	,CPL_VAL_LIQ_GAR_GEN		DECIMAL (18,4)
	,CPL_VAL_LIQ_GAR_ESP		DECIMAL (18,4)
	,CPL_CANT_PRG_TOT			INTEGER
	,CPL_CANT_REN				INTEGER
	,BCI_PROCESS_DATE 			DATE
	,EVENT_START_DT 			DATE
	,BCI_AMT1 					DECIMAL(18,4)
	,BCI_AMT2 					DECIMAL(18,4)
	,ACCOUNT_NUM 				CHAR(18)
	,ACCOUNT_MODIFIER_NUM 		CHAR(18)
	,FEC_ING_BAL				DATE
	,OPE_COP_PRO_REN 			CHAR(18)
	,NROS_PRO_REN				INTEGER
	,MARCA_PRO 					CHAR(05)
	,MARCAR_ALR 				INT
	,CPL_CORR					INT
	,FEC_EJEC					DATE
)PRIMARY INDEX (RUT, ACCOUNT_NUM, ACCOUNT_MODIFIER_NUM, EVENT_START_DT , BCI_PROCESS_DATE);
.IF ERRORCODE<>0 THEN .QUIT 8;


INSERT INTO ${BD_TEMP}.APR_CLI_DATOS_COMPLEMENT_F
SELECT 
	 U.DIAULTMES(DATE,FORMAT 'YYYYMMDD')
	,U.RUT
	,COALESCE(A.CPL_MTO_LCG_APR,0)
	,COALESCE(A.CPL_FEC_VEN_LCG,('19000101' (DATE,FORMAT 'YYYYMMDD'))) 
	,CASE WHEN A.CPL_MOD='IND' 
		THEN COALESCE(A.CPL_CAL_BCI,'S/I')
		ELSE ' '
	  END 
	,COALESCE(A.CPL_POR_SOW,0)
	,COALESCE(A.CPL_FEC_ULT_APC_RES, ('19000101' (DATE,FORMAT 'YYYYMMDD')))
	,COALESCE(A.CPL_EST_ULT_APC_RES, 'S/I')
	,COALESCE(A.CPL_FEC_ULT_APC, ('19000101' (DATE,FORMAT 'YYYYMMDD')))
	,COALESCE(A.CPL_EST_ULT_APC,'S/I')
	,COALESCE(A.CPL_FEC_ULT_ETG_EPY,('19000101' (DATE,FORMAT 'YYYYMMDD')))
	,COALESCE(A.CPL_EST_ULT_ETG_EPY,'S/I')
	,COALESCE(A.CPL_IND_CLI_COS_EPY,'I')
	,CASE WHEN A.CPL_MOD='IND' 
		THEN COALESCE(A.CPL_FEC_CAL, ('19000101' (DATE,FORMAT 'YYYYMMDD')))
		ELSE   ('19000101' (DATE,FORMAT 'YYYYMMDD')) 
	 END
	,COALESCE(A.CPL_VAL_PI,0)*100
	,COALESCE(A.CPL_MOD,'S/I')
	,COALESCE(A.CPL_SDO_PROM_MES,0)
	,COALESCE(A.CPL_MTO_VTA_MES,0)
	,COALESCE(A.CPL_VAL_LIQ_GAR_GEN,0)
	,COALESCE(A.CPL_VAL_LIQ_GAR_ESP,0)
	,COALESCE(B.CPL_CANT_PRG_TOT,0)
	,COALESCE(B.CPL_CANT_REN,0)
	,COALESCE(B.BCI_PROCESS_DATE,('1900-01-01'(DATE,FORMAT'YYYY-MM-DD')))
	,COALESCE(B.EVENT_START_DT,('1900-01-01'(DATE,FORMAT'YYYY-MM-DD')))
	,ZEROIFNULL(B.BCI_AMT1)
	,ZEROIFNULL(B.BCI_AMT2)
	,COALESCE(B.ACCOUNT_NUM,'')
	,COALESCE(B.ACCOUNT_MODIFIER_NUM,'')
	,COALESCE(B.FEC_ING_BAL,('1900-01-01'(DATE,FORMAT'YYYY-MM-DD')))
	,COALESCE(OPE_COP_PRO_REN,'')
	,ZEROIFNULL(NROS_PRO_REN)
	,COALESCE(MARCA_PRO,'') 
	,ZEROIFNULL(MARCAR_ALR)	
        ,ROW_NUMBER() OVER (order by (select 1 ))
	,${BD_TEMP}.APR_CLI_FECHA_PROC_I.FECHA_HOY

FROM
	${BD_TEMP}.APR_CLI_UNI_CLI_S U
LEFT JOIN
	${BD_TEMP}.APR_CLI_DATOS_COMPLEMENT_S A
	ON U.RUT=A.RUT
LEFT JOIN
	${BD_TEMP}.APR_CLI_REN_PRO_S B
	ON U.RUT=B.RUT
;

.IF ERRORCODE<>0 THEN .QUIT 8;


COLLECT STATISTICS USING SAMPLE
INDEX (RUT, ACCOUNT_NUM, ACCOUNT_MODIFIER_NUM, EVENT_START_DT , BCI_PROCESS_DATE)
ON ${BD_TEMP}.APR_CLI_DATOS_COMPLEMENT_F;
.IF ERRORCODE<>0 THEN .QUIT 8;


/*---------------------------------------------------------------------------------------*/
/*--		ELIMINA TABLAS		--*/
/*---------------------------------------------------------------------------------------*/

DROP TABLE ${BD_TEMP}.APR_CLI_DATOS_COMPLEMENT_S;
DROP TABLE ${BD_TEMP}.APR_CLI_REN_PRO_S;




/*------------------------------------------------------------------------------------------------------------------------------*/
SEL DATE, TIME;
/*------------------------------------------------------------------------------------------------------------------------------*/
.LOGOFF;

.QUIT 0;
