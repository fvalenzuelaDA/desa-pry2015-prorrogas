.logon 161.131.4.10/dconfsbp,dbnpp001;

.LOGTABLE edw_temp.APR_CLI_COMPLEM_EXPORT;
.BEGIN EXPORT;
.EXPORT OUTFILE APR_Complementario_201408_20140831.csv
MODE RECORD FORMAT TEXT;
SELECT
(
	(CAST((FEC_PROC							(FORMAT 'YYYYMM'))			AS VARCHAR(10)))	|| ';'	||
	(CAST((RUT								(FORMAT '--Z(18)9'))		AS VARCHAR(10)))	|| ';'	||	
	(CAST((CPL_MTO_LCG_APR					(FORMAT '--Z(18)9'))		AS VARCHAR(15)))	|| ';'	||	
	(CAST((CPL_FEC_VEN_LCG					(FORMAT 'YYYY-MM-DD'))		AS VARCHAR(10)))	|| ';'	||
	(CAST(CPL_COD_CLA_BCI												AS VARCHAR(3))) 	|| ';'	||	
	(CAST((CPL_POR_SOW						(FORMAT '--Z(18)9'))		AS VARCHAR(15)))	|| ';'	||	
	(CAST((CPL_FEC_ULT_APC_RES				(FORMAT 'YYYY-MM-DD'))		AS VARCHAR(10)))	|| ';'	||
	(CAST(CPL_EST_ULT_APC_RES											AS VARCHAR(20)))	|| ';'	||	
	(CAST((CPL_FEC_ULT_APC					(FORMAT 'YYYY-MM-DD'))		AS VARCHAR(10)))	|| ';'	||
	(CAST(CPL_EST_ULT_APC												AS VARCHAR(20)))	|| ';'	||	
	(CAST((CPL_FEC_ULT_ETG_EPY				(FORMAT 'YYYY-MM-DD'))		AS VARCHAR(10)))	|| ';'	||
	(CAST(CPL_EST_ULT_ETG_EPY											AS VARCHAR(20)))	|| ';'	||	
	(CAST(CPL_IND_CLI_COS_EPY											AS VARCHAR(20)))	|| ';'	||	
	(CAST((CPL_FEC_CAL						(FORMAT 'YYYY-MM-DD'))		AS VARCHAR(10)))	|| ';'	||
	(CAST((CPL_VAL_PI						(FORMAT '--Z(18)9.9(6)'))	AS VARCHAR(10)))	|| ';'	||	
	(CAST(CPL_MOD														AS VARCHAR(25)))	|| ';'	||	
	(CAST((CPL_SDO_PROM_MES					(FORMAT '--Z(18)9'))		AS VARCHAR(15)))	|| ';'	||	
	(CAST((CPL_MTO_VTA_MES					(FORMAT '--Z(18)9'))		AS VARCHAR(15)))	|| ';'	||	
	(CAST((CPL_VAL_LIQ_GAR_GEN				(FORMAT '--Z(18)9'))		AS VARCHAR(15)))	|| ';'	||	
	(CAST((CPL_VAL_LIQ_GAR_ESP				(FORMAT '--Z(18)9'))		AS VARCHAR(15)))	|| ';'	||	
	(CAST((CPL_CANT_PRG_TOT					(FORMAT '--Z(18)9'))		AS VARCHAR(10)))	|| ';'	||	
	(CAST((CPL_CANT_REN						(FORMAT '--Z(18)9'))		AS VARCHAR(10)))	|| ';'	||
	(CAST((FEC_EJEC							(FORMAT 'YYYY-MM-DD'))		AS VARCHAR(10)))	|| ';'	||
	(CAST((BCI_PROCESS_DATE					(FORMAT 'YYYY-MM-DD'))		AS VARCHAR(10)))	|| ';'	||	
	(CAST((EVENT_START_DT					(FORMAT 'YYYY-MM-DD'))		AS VARCHAR(10)))	|| ';'	||
	(CAST(ACCOUNT_NUM													AS VARCHAR(18)))	|| ';'	||
	(CAST(ACCOUNT_MODIFIER_NUM											AS VARCHAR(18)))	|| ';'	||
	(CAST((BCI_AMT1							(FORMAT '--Z(18)9'))		AS VARCHAR(10)))	|| ';'	||
	(CAST((BCI_AMT2							(FORMAT '--Z(18)9'))		AS VARCHAR(10)))	|| ';'	||
	(CAST((FEC_ING_BAL						(FORMAT 'YYYY-MM-DD'))		AS VARCHAR(10)))	|| ';'	||
	(CAST(OPE_COP_PRO_REN												AS VARCHAR(18)))	|| ';'	||
	(CAST((NROS_PRO_REN						(FORMAT '--Z(18)9'))		AS VARCHAR(10)))	|| ';'	||
	(CAST(MARCA_PRO														AS VARCHAR(5)))		|| ';'	||
	(CAST((MARCAR_ALR						(FORMAT '--Z(18)9'))		AS VARCHAR(10)))	|| ';'	||
	(CAST((CPL_CORR							(FORMAT '--Z(18)9'))		AS VARCHAR(10)))	|| ''
	)
	(CHAR (570))(TITLE '')
 FROM 
	edw_temp.APR_CLI_DATOS_COMPLEMENT_F

;
.END EXPORT;

SEL DATE, TIME;

.LOGOFF;
.QUIT 0;