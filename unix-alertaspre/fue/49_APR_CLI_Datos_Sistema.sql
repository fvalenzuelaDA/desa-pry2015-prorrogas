.logon usuario,password;

SEL DATE, TIME;

/* **************************************************************************************************** */
/*																										*/
/*Nombre Bteq				:49_APR_CLI_Datos_Sistema.sql												*/
/*Nombre BD					:EDW_VW																		*/
/*Tipo de ejecucion			:Batch																		*/
/*Fecha de creacion			:05/03/2016																	*/
/*Autor						:Enrique S. --ADA LTDA														*/
/*Objetivos					:Se extrae la informacion de protestos del sistema, infracciones laborales 	*/
/*							 y morosos del comercio														*/
/*Canal de ejecucion		:Control-M																	*/
/*Parametros de entrada		:${BD_VW}																	*/
/*							 ${BD_TEMP}																	*/
/*Retorno					:Informacion de protestos del sistema, infracciones laborales 				*/
/*							 y morosos del comercio														*/
/*Ejemplo de ejecucion		:No aplica, ya que es ejecutado por medio de una shell						*/
/* **************************************************************************************************** */
/*																										*/
/*Fecha ultima modificacion :01/09/2021																	*/
/*Autor modificacion		:Francisco V. --ADA LTDA													*/
/*Objetivo modificacion		:Extraccion de nueva informacion de protesto (monto y cantidad), 			*/
/*							 mora comercio (monto, cantidad, fecha) e infracciones laborales 			*/
/*							(monto, meses deuda)														*/
/* **************************************************************************************************** */

/*------------------------------------------------------------------------------------------------------------------------
PROTESTOS DEL SISTEMA
------------------------------------------------------------------------------------------------------------------------*/
--PROTESTOS DE SISTEMA:
DROP TABLE ${BD_TEMP}.APR_CLI_PRT_SISTEMA;
CREATE SET TABLE ${BD_TEMP}.APR_CLI_PRT_SISTEMA ,NO FALLBACK ,
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT,
     DEFAULT MERGEBLOCKRATIO
     (
      DIAULTMES DATE FORMAT 'YY/MM/DD',
      RUT INTEGER,
      Party_Id INTEGER,
      pst_num_prt_vig_m1 INTEGER,
      pst_mto_prt_vig_m1 DECIMAL(18,0))
UNIQUE PRIMARY INDEX ( RUT );


INSERT INTO ${BD_TEMP}.APR_CLI_PRT_SISTEMA
SELECT
	B.DIAULTMES,
	B.RUT,
	B.PARTY_ID,
	( A.pst_num_prt_vig_m1 + A.pst_num_prt_vig_m234 ), -- 01062021 CANTIDAD PROTESTOS VIGENTES MES+ANTERIORES  
	( A.pst_mto_prt_vig_m1 + A.pst_mto_prt_vig_m234 )  -- MONTO EN UF DE PROTESTOS VIGENTES MES+ANTERIORES 
FROM
	${BD_TEMP}.APR_CLI_ARCHIVO_PRTSIST_I A,
	${BD_TEMP}.APR_CLI_UNI_CLI_S	B
WHERE
	A.PST_RUT=B.RUT
AND ( A.pst_num_prt_vig_m1 + A.pst_num_prt_vig_m234 ) > 0
;

.IF ERRORCODE<>0 THEN .QUIT 8;

COLLECT STATISTICS USING SAMPLE
INDEX ( RUT)
ON ${BD_TEMP}.APR_CLI_PRT_SISTEMA;
.IF ERRORCODE<>0 THEN .QUIT 8;


--PROTESTOS DE SISTEMA: EN PESOS A FIN DE MES DE LA FECHA PROCESO
DROP TABLE ${BD_TEMP}.APR_CLI_PRT_SISTEMA_PSO;
CREATE SET TABLE ${BD_TEMP}.APR_CLI_PRT_SISTEMA_PSO ,NO FALLBACK ,
     NO BEFORE JOURNAL,
     NO AFTER JOURNAL,
     CHECKSUM = DEFAULT,
     DEFAULT MERGEBLOCKRATIO
     (
      DIAULTMES DATE FORMAT 'YY/MM/DD',
      RUT INTEGER,
      Party_Id INTEGER,
      pst_num_prt_vig_m1 INTEGER,
      pst_mto_prt_vig_m1 DECIMAL(18,0),
      pst_mto_prt_vig_m1_pso DECIMAL(18,0))
UNIQUE PRIMARY INDEX ( RUT );


INSERT INTO ${BD_TEMP}.APR_CLI_PRT_SISTEMA_PSO 
SELECT
	A.DIAULTMES,
	A.RUT,
	A.PARTY_ID,
	A.pst_num_prt_vig_m1, --CANTIDAD PROTESTOS VIGENTES MES  
	A.pst_mto_prt_vig_m1, --MONTO EN UF DE PROTESTOS VIGENTES MES 
	(A.pst_mto_prt_vig_m1 * C.Global_To_Source_Currency_Rate (DECIMAL (18,0))) AS pst_mto_prt_vig_m1_pso
FROM
	${BD_TEMP}.APR_CLI_PRT_SISTEMA A
LEFT JOIN
	${BD_VW}.CURRENCY_TRANSLATION_RATE_HIST C
ON	TRIM(C.Global_Currency_Cd) = '0998'
	AND	C.Currency_Trans_Start_Dt = A.DIAULTMES 
	AND	C.Source_Currency_Cd = '0999'
	AND	C.Currency_Trans_End_Dt IS NULL
;

.IF ERRORCODE<>0 THEN .QUIT 8;

COLLECT STATISTICS USING SAMPLE
INDEX ( RUT)
ON ${BD_TEMP}.APR_CLI_PRT_SISTEMA_PSO;
.IF ERRORCODE<>0 THEN .QUIT 8;


/*------------------------------------------------------------------------------------------------------------------------
INFRACCIONES LABORALES: cantidad de infracciones laborales por no pago previsional, sin incluir multa
------------------------------------------------------------------------------------------------------------------------*/
DROP TABLE  ${BD_TEMP}.APR_CLI_INFRACCIONES_LAB ;  
CREATE TABLE ${BD_TEMP}.APR_CLI_INFRACCIONES_LAB (
	DIAULTMES DATE
	,RUT INTEGER
	,Cant_Inf_Lab_NPP INTEGER
	,MTO_INF_LAB_NPP DECIMAL(18,4)
	,MAX_MESES_DDA_LAB_NPP INT
)UNIQUE PRIMARY INDEX(RUT) ;

.IF ERRORCODE<>0 THEN .QUIT 8;

--AGREGA INFORAMACION: 201409: NO TENGO ARCHIVOS REALES PARA VALIDAR LA CALIDAD DE INFORMACION
INSERT INTO ${BD_TEMP}.APR_CLI_INFRACCIONES_LAB
SELECT
	B.DIAULTMES
	,B.RUT
	,COUNT(A.b21_num_boletin) as Cant_Inf_Lab_NPP --cantidad de infracciones laborales por no pago previsional, sin incluir multas
	,SUM(b21_monto) AS MTO_INF_LAB_NPP ----Monto de infracciones laborales por no pago previsional, sin incluir multas
	,MAX(b21_num_meses_adeudado) AS MAX_MESES_DDA_LAB_NPP-- Maximo de meses endeudado
FROM 
	${BD_TEMP}.APR_CLI_INFRAC_LAB_I A,
	${BD_TEMP}.APR_CLI_UNI_CLI_S B
WHERE
	A.b21_rut_cli=B.RUT
AND A.b21_tipo_infraccion NOT IN ('M') --no incluye multas
AND  A.b21_motivo_infraccion IN ('DECLARACIONES SIN PAGO', 'IMPOSICIONES NO DECLARADAS')
GROUP BY 1,2; 

.IF ERRORCODE<>0 THEN .QUIT 8;


COLLECT STATISTICS USING SAMPLE
INDEX ( RUT)
ON ${BD_TEMP}.APR_CLI_INFRACCIONES_LAB;
.IF ERRORCODE<>0 THEN .QUIT 8;


/*------------------------------------------------------------------------------------------------------------------------
MOROSOS COMERCIO:
Cantidad  Y Monto Morosos del Comercio, sin considerar las moras de l�neas de celulares, ni las moras en las autopistas concesionadas

FALTA VALIDAR PARA FILTRAR LAS MORAS DE LINEAS DE CELULARES Y AUTOPISTAS
FALTA TRASNFORMAR A PESOS, YA QUE HAY VALORES EN OTRAS MONEDAS
------------------------------------------------------------------------------------------------------------------------*/
DROP TABLE  ${BD_TEMP}.APR_CLI_MOROSOS_COM ;  
CREATE TABLE ${BD_TEMP}.APR_CLI_MOROSOS_COM (
	DIAULTMES 			DATE
	,RUT 				INTEGER
	,Cant_Mor_Com 		INTEGER
	,Mto_Mor_Com		DECIMAL(18,0)
	,FEC_MAX_MOR_COM	DATE
)UNIQUE PRIMARY INDEX(RUT) ;

.IF ERRORCODE<>0 THEN .QUIT 8;

--AGREGA INFORAMACION: 20140911: NO TENGO ARCHIVO PARA HACER PRUEBAS. SE DEBE PROGRAMAR ESTOS DATOS
INSERT INTO ${BD_TEMP}.APR_CLI_MOROSOS_COM
SELECT
	B.DIAULTMES
	,B.RUT
	,COUNT(A.MC_Rut_DD) as Cant_Mor_Com
	,SUM(A.MC_Mto_Deu_Mor) as Mto_Mor_Com 
	,MAX(MC_Fec_Ven) AS FEC_MAX_MOR_COM
FROM 
	${BD_TEMP}.APR_CLI_MOROSO_COMERCIO_I A,
	${BD_TEMP}.APR_CLI_UNI_CLI_S B
WHERE
	A.MC_Rut=B.RUT
AND A.MC_Desc_Inst NOT LIKE ALL ('%autop%','%costanera%','%vespucio%','%vespucio%','%ruta%','%telef%','%entel%','%movistar%','%claro%','%nextel%','%vtr%','%telsur%','%gtd%','%cmet%')
GROUP BY 1,2;

.IF ERRORCODE<>0 THEN .QUIT 8;

COLLECT STATISTICS USING SAMPLE
INDEX ( RUT)
ON ${BD_TEMP}.APR_CLI_MOROSOS_COM;
.IF ERRORCODE<>0 THEN .QUIT 8;


/*----------------------------------------------------------------------------------------------*/
/*	TABLA DE SALIDA  DATOS SISTEMA							*/
/*----------------------------------------------------------------------------------------------*/
DROP TABLE  ${BD_TEMP}.APR_CLI_DATOS_SBIF_S ;  
CREATE TABLE ${BD_TEMP}.APR_CLI_DATOS_SBIF_S 
(
	 FEC_PROC					DATE
	,PARTY_ID					INTEGER
	,RUT						INTEGER
	,PSB_CANT_VIG_MES			INTEGER
	,PSB_MTO_PRT_VIG_MES		DECIMAL (18,4)
	,PSB_CANT_INF_LAB			INTEGER
	,PSB_MTO_INF_LAB			DECIMAL(18,4)
	,PSB_MAX_MESES_DDA_LAB 		INTEGER
	,PSB_CANT_MOR_COM			INTEGER
	,PSB_MTO_MOR_COM			DECIMAL (18,4)
	,PSB_MAX_FEC_MOR_COM		DATE
) UNIQUE PRIMARY INDEX(RUT);

.IF ERRORCODE<>0 THEN .QUIT 8;


INSERT INTO ${BD_TEMP}.APR_CLI_DATOS_SBIF_S
SELECT
	 U.DIAULTMES
	,U.PARTY_ID
	,U.RUT
	,COALESCE(A.pst_num_prt_vig_m1,0)
	,COALESCE(A.pst_mto_prt_vig_m1_pso,0)
	,COALESCE(B.Cant_Inf_Lab_NPP,0)
	,COALESCE(B.MTO_INF_LAB_NPP,0)
	,COALESCE(B.MAX_MESES_DDA_LAB_NPP,0)
	,COALESCE(C.Cant_Mor_Com,0)
	,COALESCE(C.Mto_Mor_Com,0)
	,COALESCE(C.FEC_MAX_MOR_COM, ('19000101'(DATE,FORMAT 'YYYYMMDD')))
FROM
	${BD_TEMP}.APR_CLI_UNI_CLI_S		U
LEFT JOIN
	${BD_TEMP}.APR_CLI_PRT_SISTEMA_PSO	A
	ON U.RUT=A.RUT
LEFT JOIN
	${BD_TEMP}.APR_CLI_INFRACCIONES_LAB B
	ON U.RUT=B.RUT
LEFT JOIN
	${BD_TEMP}.APR_CLI_MOROSOS_COM C
	ON U.RUT=C.RUT
;

.IF ERRORCODE<>0 THEN .QUIT 8;

COLLECT STATISTICS USING SAMPLE
INDEX ( RUT)
ON ${BD_TEMP}.APR_CLI_DATOS_SBIF_S;
.IF ERRORCODE<>0 THEN .QUIT 8;



/*----------------------------------------------------------------------------------------------*/
/*			Elimina Tabla																		*/
/*----------------------------------------------------------------------------------------------*/

DROP TABLE 	${BD_TEMP}.APR_CLI_PRT_SISTEMA;
DROP TABLE 	${BD_TEMP}.APR_CLI_PRT_SISTEMA_PSO;
DROP TABLE  ${BD_TEMP}.APR_CLI_INFRACCIONES_LAB ;  
DROP TABLE  ${BD_TEMP}.APR_CLI_MOROSOS_COM ;  
DROP TABLE  ${BD_TEMP}.APR_CLI_INFRAC_LAB_I;
DROP TABLE  ${BD_TEMP}.APR_CLI_ARCHIVO_PRTSIST_I;
DROP TABLE  ${BD_TEMP}.APR_CLI_MOROSO_COMERCIO_I ;

SEL DATE, TIME;
.LOGOFF
.QUIT 0;

