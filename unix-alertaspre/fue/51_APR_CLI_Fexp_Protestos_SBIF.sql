.logon usuario,password;


/* **************************************************************************************************** */
/*																										*/
/*Nombre Bteq				:51_APR_CLI_Fexp_Protestos_SBIF.sql											*/
/*Nombre BD					:EDW_VW																		*/
/*Tipo de ejecucion			:Batch																		*/
/*Fecha de creacion			:05/03/2016																	*/
/*Autor						:Enrique S. --ADA LTDA														*/
/*Objetivos					:Se exporta la informacion de cliente obtenido en procesos anteriores, 		*/
/*							con la informacion de protestos del sistema, infracciones laborales 		*/
/*							y morosos del comercio a un archivo plano									*/
/*Canal de ejecucion		:Control-M																	*/
/*Parametros de entrada		:${BD_VW}																	*/
/*							 ${BD_TEMP}																	*/
/*Retorno					:Clientes con informacion de protestos del sistema, infracciones laborales  */
/*							 y morosos del comercio														*/
/*Ejemplo de ejecucion		:No aplica, ya que es ejecutado por medio de una shell						*/
/* **************************************************************************************************** */
/*																										*/
/*Fecha ultima modificacion :01/09/2021																	*/
/*Autor modificacion		:Francisco V. --ADA LTDA													*/
/*Objetivo modificacion		:Se agregan al archivo de salida los nuevos campos obtenido en el proceso 	*/
/*							 anterior de protesto (monto y cantidad), mora comercio (monto, cantidad, 	*/
/*							 fecha) e infracciones                     									*/
/*							 laborales (monto, meses deuda)												*/
/* **************************************************************************************************** */

.LOGTABLE ${BD_TEMP}.APR_CLI_PROTESTOS_SBF_EXPORT;
.BEGIN EXPORT;
.EXPORT OUTFILE $ARCH_PROT_SBIF
MODE RECORD FORMAT TEXT;
SELECT
(
(CAST((FEC_PROC					(FORMAT 'YYYYMM'))			AS VARCHAR(10)))	|| ';'	||
(CAST((RUT						(FORMAT '--Z(18)9'))		AS VARCHAR(10)))	|| ';'	||	
(CAST((PSB_CANT_PRT_FF			(FORMAT '--Z(18)9'))		AS VARCHAR(5)))		|| ';'	||
(CAST((PSB_MTO_PRT_FF			(FORMAT '--Z(18)9'))		AS VARCHAR(15)))	|| ';'	||
(CAST((PSB_CANT_MOR_COM			(FORMAT '--Z(18)9'))		AS VARCHAR(5)))		|| ';'	||
(CAST((PSB_MTO_MOR_COM			(FORMAT '--Z(18)9.9(2)'))	AS VARCHAR(18)))  	|| ';'  ||
(CAST((PSB_CANT_INF_LAB			(FORMAT '--Z(18)9'))		AS VARCHAR(5)))		|| ';'	||
(CAST((FEC_EJEC					(FORMAT 'YYYY-MM-DD'))		AS VARCHAR(10)))	|| ';'	||
(CAST((PSB_MAX_FEC_MOR_COM		(FORMAT 'YYYY-MM-DD'))		AS VARCHAR(10)))	|| ';'	||
(CAST((PSB_MTO_INF_LAB			(FORMAT '--Z(18)9.9(2)'))	AS VARCHAR(18)))  	|| ';'  ||
(CAST((PSB_MAX_MESES_DDA_LAB	(FORMAT '--Z(18)9'))		AS VARCHAR(10)))	|| ''	
)
	(CHAR (200))(TITLE '')
 FROM 
	${BD_TEMP}.APR_CLI_PROTESTOS_SBF_F

;
.END EXPORT;

SEL DATE, TIME;


.LOGOFF;
.QUIT 0;

	